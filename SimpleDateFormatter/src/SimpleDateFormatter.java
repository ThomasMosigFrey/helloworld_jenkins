import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateFormatter {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("s:S");
		System.out.println(sdf.format(new Date()));
	}

}
